import { Component , OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
	selector: 'app-home',
	templateUrl: 'home.page.html',
	styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

	constructor(private httpClient: HttpClient) {}

	// private urlapi = 'http://apidce.azurewebsites.net/api/GEOParques';
	private urlapi = 'assets/data/GEOParques.json';
	public reponseService: any = null;

	private getData (){
		this.httpClient
			.get(this.urlapi)
			.subscribe( apiData => {
				console.log(apiData);
				this.reponseService = apiData;
			} )
		// this.httpClient.jsonp(this.urlapi , 'callback')
	}

	ngOnInit() {
		this.getData();
	}

	lat: number = 4.594041008836859;
	lng: number = -74.12210804902344;
	zoom: number = 11;
	apiMarkers = '';
	markers = [
		{
			lat: 4.660399274411327,
			lng: -74.07460054288333
		},
		{
			lat: 4.667200221112848,
			lng: -74.05914029012149
		},
		{
			lat: 4.649791377150661,
			lng: -74.06284173856204
		},
		{
			lat: 4.650219118036521,
			lng: -74.0752442730469
		},
	]
}
