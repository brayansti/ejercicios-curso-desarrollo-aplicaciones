-- Database: parques

-- DROP DATABASE parques;

CREATE DATABASE parques
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Spanish_Spain.1252'
    LC_CTYPE = 'Spanish_Spain.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

CREATE TABLE rol(
	id_rol INT PRIMARY KEY,
	descrip_rol VARCHAR(45),
	estado_rol INT
);

CREATE TABLE tipo_documento(
	tipo_documento VARCHAR(15) PRIMARY KEY,
	descrip_doc VARCHAR(45),
	estado_tdoc INT
);

CREATE TABLE persona(
	id_pers INT PRIMARY KEY,
	nom_pers VARCHAR(45),
	apell_pers VARCHAR(45),
	correo_pers VARCHAR(45),
	rol_id_rol INT,
	pfk_tdoc VARCHAR(15)
);

INSERT INTO tipo_documento VALUES ('cc' , 'cedula' , 123456789);
INSERT INTO persona VALUES (1 , 'Brayan' , 'Camargo' , 'brayan123@gmail.com' , 1 , 'admin');

SELECT * FROM persona