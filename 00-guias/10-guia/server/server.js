'use strict'

// web server
const http = require('http');
// Express ↓↓
const express = require('express');
// Colores Consola ↓↓
const chalk = require('chalk');

// Puerto ↓↓
const port = process.env.PORT || 3000;
// Express aplicacion ↓↓
const app = express();
// Instancia del servidor ↓↓
const server = http.createServer(app);
// Instancia del servidor ↓↓
const api = require('./api');

// ↓↓ RUN middleware
app.use('/api' , api);

// ↓↓ Manejo errores
app.use( ( err , req ,res, next )=>{
    const errorMessage = err.message;
    debug(`ERROR: ${errorMessage}`);
    if( errorMessage.match(/not found/) ){
        return res.status(404).send( {error: errorMessage} )
    }
    res.status(500).send({errorMessage});
} );
function handleFatalError (err){
    console.error(`${chalk.red('[FATAL ERROR]')} ${err.message} `);
    console.error(err.stack);
    process.exit(1);
}
process.on('uncaughtException' , handleFatalError)
process.on('uncaughtRejection' , handleFatalError)


// Listen port ↓↓
server.listen(port , ()=>{
    console.log(` ${chalk.green(['CUSTOM SERVER REST']) } Server listening on port ${port}`);    
});