<!DOCTYPE html>
<html lang="en">
<?php include 'includes/head.php'; ?>
<body>
	<?php include 'includes/header.php'; ?>
	<div class="container">
		<div class="row mt20">
			<div class="col">
				<img class="d-block img100" src="img/zaraBanner.jpg" alt="BANNER">
			</div>
		</div>
		<div class="blackBox">
			<div class="row align-items-center">
				<div class="col-12 col-lg-10 pt10 pb10">
					<p class="pl15">
						Lorem ipsum dolor sit amet consectetur adipisicing elit...
					</p>
				</div>
				<div class="col-12 col-lg-2 pt10 pb10">
					<button class="btnRed">
						See article
					</button>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<main class="mainContent">
			<div class="row">
				<div class="col-lg-8">
					<div class="title_icon titulo1">
						<div class="icon">
							<img src="img/zozor_logo.png" alt="">
						</div>
						I´M A GREAT TRAVELLER
					</div>
					<div class="mt20">
						<p>
							Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos asperiores, tenetur fuga, magnam itaque quasi nesciunt odio officiis numquam at corrupti, incidunt perspiciatis doloremque! Est ut numquam vel porro assumenda.
						</p>
						<p>
							Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos asperiores, tenetur fuga, magnam itaque quasi nesciunt odio officiis numquam at corrupti, incidunt perspiciatis doloremque! Est ut numquam vel porro assumenda.
						</p>
						<p>
							Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos asperiores, tenetur fuga, magnam itaque quasi nesciunt odio officiis numquam at corrupti, incidunt perspiciatis doloremque! Est ut numquam vel porro assumenda.
						</p>
						<p>
							Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos asperiores, tenetur fuga, magnam itaque quasi nesciunt odio officiis numquam at corrupti, incidunt perspiciatis doloremque! Est ut numquam vel porro assumenda.
						</p>
						<p>
							Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos asperiores, tenetur fuga, magnam itaque quasi nesciunt odio officiis numquam at corrupti, incidunt perspiciatis doloremque! Est ut numquam vel porro assumenda.
						</p>
						<p>
							Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quos asperiores, tenetur fuga, magnam itaque quasi nesciunt odio officiis numquam at corrupti, incidunt perspiciatis doloremque! Est ut numquam vel porro assumenda.
						</p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="grayBox mt20">
						<h1 class="taC mt10">
							ABOUT THE AUTOR
						</h1>
						<div class="grayBox__image mt20">
							<img src="img/zozor.png" alt="ZARA">
						</div>
						<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui harum dolores sapiente dicta, soluta, at incidunt, fuga maiores nulla non suscipit laborum. Commodi placeat illo dicta nulla aliquam recusandae voluptatem.
						</p>
						<div class="grayBox__social">
							<a href="">
								<img src="img/facebook.png" alt="">
							</a>
							<a href="">
								<img src="img/flickr.png" alt="">
							</a>
							<a href="">
								<img src="img/twitter.png" alt="">
							</a>
							<a href="">
								<img src="img/vimeo.png" alt="">
							</a>
							<a href="">
								<img src="img/rss.png" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</main>
	</div>
	<?php include 'includes/footer.php'; ?>
</body>
</html>
