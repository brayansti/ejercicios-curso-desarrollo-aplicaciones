<footer class="mainFooter">
    <div class="container mt30">
        <div class="row">
            <div class="col-12 col-lg-4">
                <h3 class="titulo1">
                    MY LAST TWEET
                </h3>
                <p>Hee-hwal</p>
                <p>10/02/2019</p>
            </div>
            <div class="col-12 col-lg-4">
                <h3 class="titulo1">
                    MY VISITORS
                </h3>
                <div class="mt10">
                    <img src="img/pic1.jpg" alt="ciudad">
                    <img src="img/pic2.jpg" alt="ciudad">
                    <img src="img/pic3.jpg" alt="ciudad">
                    <img src="img/pic4.jpg" alt="ciudad">
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <h3 class="titulo1">
                    MY FRIENDS
                </h3>

                <div class="row no-gutters">
                    <div class="col-6">
                        <ul class="listaRoja">
                            <li>Lorem ipsum dolor sit amet consectetur</li>
                            <li>Lorem ipsum dolor sit amet consectetur</li>
                            <li>Lorem ipsum dolor sit amet consectetur</li>
                            <li>Lorem ipsum dolor sit amet consectetur</li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="listaRoja">
                            <li>Lorem ipsum dolor sit amet consectetur</li>
                            <li>Lorem ipsum dolor sit amet consectetur</li>
                            <li>Lorem ipsum dolor sit amet consectetur</li>
                            <li>Lorem ipsum dolor sit amet consectetur</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<script
    src="https://code.jquery.com/jquery-2.2.4.min.js"
    integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous">
</script>
<script src="js/mainscript.js"></script>
