<header class="mainHeader">
    <div class="container">
        <div class="mainHeader__logo">
            <img src="img/logo.jpg" alt="">
        </div>
        <div class="mainHeader__menu">
            <nav class="mainHeader__nav">
                <ul>
                    <li>
                        <a href="#">HOME</a>
                    </li>
                    <li>
                        <a href="#">BLOG</a>
                    </li>
                    <li>
                        <a href="#">RESUMEN</a>
                    </li>
                    <li>
                        <a href="#">CONTACTO</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
