import { Component } from '@angular/core';

@Component({
	selector: 'app-tab1',
	templateUrl: 'tab1.page.html',
	styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

    latinCountries = [
		{
			name: 'Colombia',
			capilal: 'Bogotá',
			population: 65000000,
		},
		{
			name: 'Ecuador',
			capilal: 'Quito',
			population: 70000000,
		},
		{
			name: 'Argentina',
			capilal: 'Buenos Aites',
			population: 50000000,
		}
	]
	
}
