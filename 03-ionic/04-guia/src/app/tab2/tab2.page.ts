import { Component } from '@angular/core';

@Component({
	selector: 'app-tab2',
	templateUrl: 'tab2.page.html',
	styleUrls: ['tab2.page.scss']
})
export class Tab2Page {

	citySelect = [];
	description:string = '';

	citys = [
		{
			name: 'Bogotá',
			slug: 'bogota'
		},
		{
			name: 'Cartagena',
			slug: 'cartagena'
		},
		{
			name: 'Medellín',
			slug: 'medellin'
		},
		{
			name: 'Barranquilla',
			slug: 'barranquilla'
		},
		{
			name: 'Cali',
			slug: 'cali'
		},
	]

	colombianSites = [];
	// ↓↓ Click ↓↓
	pushColombianSites() {
		let validate = this.citySelect[0] === '' || this.description === '' ? false : true;

		if(validate){
			this.colombianSites.push({
				name: this.citySelect[0],
				slug: this.citySelect[1],
				description: this.description
			});
		}
		else{
			alert('Por favor ingresa ambos datos');
		}
	}

}
