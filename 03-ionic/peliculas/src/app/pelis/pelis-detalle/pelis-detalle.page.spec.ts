import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PelisDetallePage } from './pelis-detalle.page';

describe('PelisDetallePage', () => {
  let component: PelisDetallePage;
  let fixture: ComponentFixture<PelisDetallePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PelisDetallePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PelisDetallePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
