import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PelisListPage } from './pelis-list.page';

describe('PelisListPage', () => {
  let component: PelisListPage;
  let fixture: ComponentFixture<PelisListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PelisListPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PelisListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
