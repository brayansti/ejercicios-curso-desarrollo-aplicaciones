import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'pelis-list', pathMatch: 'full' },
  { path: 'pelis-list', loadChildren: './pelis/pelis-list/pelis-list.module#PelisListPageModule' },
  { path: 'pelis-detalle', loadChildren: './pelis/pelis-detalle/pelis-detalle.module#PelisDetallePageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
